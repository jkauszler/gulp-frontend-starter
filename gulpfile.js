var gulp       = require('gulp'),
    gutil      = require('gulp-util'),
    compass    = require('gulp-compass'),
    sass       = require('gulp-ruby-sass'),
    coffee     = require('gulp-coffee'),
    gulpif     = require('gulp-if'),
    uglify     = require('gulp-uglify'),
    minifyHTML = require('gulp-minify-html'),
    jsonminify = require('gulp-jsonminify'),
    imagemin   = require('gulp-imagemin'),
    pngcrush   = require('imagemin-pngcrush'),
    concat     = require('gulp-concat'),
    livereload = require('gulp-livereload'),
    lr         = require('tiny-lr'),
    server     = lr();

var env,
    jsSources,
    sassSources,
    htmlSources,
    jsonSources,
    outputDir,
    coffeeSources,
    sassStyle;

env = process.env.NODE_ENV;

if (env==='production') {
  outputDir = 'builds/production/';
  sassStyle = 'compressed';
} else {
  outputDir = 'builds/development/';
  sassStyle = 'expanded';
}

coffeeSources = ['components/coffee/*.coffee'];
jsSources     = ['components/scripts/*.js'];
sassSources   = ['components/sass/style.scss'];
htmlSources   = [outputDir + '*.html'];
jsonSources   = [outputDir + 'js/*.json'];

gulp.task('js', function() {
  gulp.src(jsSources)
    .pipe(concat('script.js'))
    .pipe(gulpif(env === 'production', uglify()))
    .pipe(gulp.dest(outputDir + 'js'))
});

gulp.task('sass', function(){
  gulp.src(sassSources)
  .pipe(sass({style: sassStyle, lineNumbers: true})
    .on('error', gutil.log))
  .pipe(concat('style.css'))
  .pipe(gulp.dest(outputDir + 'css/'))
  gutil.log(sassStyle, outputDir);
});

gulp.task('coffee', function(){
  gulp.src(coffeeSources)
    .pipe(coffee({ bare: true })
      .on('error', gutil.log))
    .pipe(gulp.dest('components/scripts'))
});

gulp.task('watch', function() {
  var server = livereload();
  gulp.watch(coffeeSources, ['coffee']);
  gulp.watch(jsSources, ['js']);
  gulp.watch('components/sass/*.scss', ['sass']);
  gulp.watch('builds/development/*.html', ['html']);
  gulp.watch('builds/development/js/*.json', ['json']);
  gulp.watch('builds/development/images/**/*.*', ['images']);
  gulp.watch([
    'builds/development/js/script.js', 
    'builds/development/css/style.css',
    'builds/development/*.html'
    ], function(e){
    server.changed(e.path);
  });
});

gulp.task('html', function() {
  gulp.src('builds/development/*.html')
    .pipe(gulpif(env === 'production', minifyHTML()))
    .pipe(gulpif(env === 'production', gulp.dest(outputDir)))
});

gulp.task('images', function() {
  gulp.src('builds/development/images/**/*.*')
    .pipe(gulpif(env === 'production', imagemin({
      progressive: true,
      svgoPlugins: [{ removeViewBox: false }],
      use: [pngcrush()]
    })))
    .pipe(gulpif(env === 'production', gulp.dest(outputDir + 'images')))
});

gulp.task('json', function() {
  gulp.src('builds/development/js/*.json')
    .pipe(gulpif(env === 'production', jsonminify()))
    .pipe(gulpif(env === 'production', gulp.dest('builds/production/js')))
});

gulp.task('default', ['html', 'json', 'js', 'sass', 'coffee', 'images', 'watch']);